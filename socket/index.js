import * as config from "./config";
import texts from "../data";

const rooms = ["ROOM_1", "Room_2", "Room_3", "Room_4", "Room_5", "Room_6"];

let timer = setInterval(()=>{},10000000);

const roomsMap = new Map(
  rooms.map(roomId => 
    [roomId, {
        maxPlayers: config.MAXIMUM_USERS_FOR_ONE_ROOM,
        players: [], 
        isGameStarted: false
      }
    ]
  ))
const userList = new Set();

const getCurrentRoomId = socket => Object.keys(socket.rooms).find(roomId => roomsMap.has(roomId));

const createRoom = (socket, roomName) => {
  roomsMap.set(roomName, {
    players: [
      {
        userId: socket.id, 
        username: socket.handshake.query.username,
        status: "notReady",
        progress: 0,
        time: 0
      }
    ],
    maxPlayers: config.MAXIMUM_USERS_FOR_ONE_ROOM, 
    isGameStarted: false
  })

  return roomsMap.get(roomName);
};

const leaveRoom = (socket, prevRoomId) => {
  const prevRoom = roomsMap.get(prevRoomId);
  roomsMap.set(prevRoomId, {
    ...prevRoom, 
    players: 
      prevRoom
      .players
      .filter(player=> player.username!==socket.handshake.query.username)
    }
  )
}

const updateRoomsListToAll = socket => {
  socket.emit("Login Response", {
    message: {
      roomsList: [... roomsMap.entries()]
    }
  });
  socket.broadcast.emit("Login Response", {
    message: {
      roomsList: [... roomsMap.entries()]
    }
  });
}

const updateSelectedRoomToAll = (io, socket, roomName, playerLeaving=false) => {
  const newRoomState = roomsMap.get(roomName);

  !playerLeaving && socket.emit("Selected Room update", {
    message: {
      room: newRoomState,
      roomName
    }
  });
  io.to(roomName).emit("Selected Room update", {
    message: {
      room: newRoomState,
      roomName
    }
  });

  if(!playerLeaving && newRoomState.players.every(player=> player.status==="ready")){
    let awaitTime = config.SECONDS_TIMER_BEFORE_START_GAME;
    let gameTime = config.SECONDS_FOR_GAME;
    const randomNumber = Math.floor(Math.random()*texts.texts.length);
    timer = setInterval(()=>{
      if(awaitTime>0){
        io.to(roomName).emit("Set start game",{message:"set", time: --awaitTime})
        socket.emit("Set start game",{message:"set", time: awaitTime})
      }
      else {
        roomsMap.set(roomName, {...newRoomState, isGameStarted: true});
        updateRoomsListToAll(socket);
        io.to(roomName).emit("Set start game",{message:"start", time: --gameTime, text: texts.texts[randomNumber]})
        socket.emit("Set start game",{message:"start", time: gameTime, text: texts.texts[randomNumber]});
 
        if(gameTime<=0) { 
          clearInterval(timer);
          roomsMap.set(roomName, {...newRoomState, isGameStarted: false});
          updateRoomsListToAll(socket);
          io.to(roomName).emit("Set start game",{message:"finish"})
          socket.emit("Set start game",{message:"finish"})
        }
      }
    },1000)
  }
  else {
    clearInterval(timer);
    io.to(roomName).emit("Set start game",{message:"pause"})
    socket.emit("Set start game",{message:"pause"})
  }
};


export default io => {
  io.on("connection", socket => {
    const username = socket.handshake.query.username;

    if(userList.has(username)) {
      socket.emit("Login Error", {message: `${username} is already exist`});
      socket.disconnect()
    }
    else {
      userList.add(username);
      updateRoomsListToAll(socket);
    }

    socket.on("Join room request", (request)=> {
      const prevRoomId = getCurrentRoomId(socket);
      const newRoom = roomsMap.get(request.message.id);
      if(newRoom.isGameStarted) {
        socket.emit("Error", {message: `${request.message.id} - game already started`});
        return;
      }
      if (request.message.id === prevRoomId){
        socket.emit("Join room response", 
          {
            message: {
              room: newRoom,
              roomName: request.message.id
            }
          })
        return;
      }
      if(prevRoomId) {
        leaveRoom(prevRoomId);
        socket.leave(prevRoomId);
      }
      if(newRoom.players.length< newRoom.maxPlayers){
        socket.join(request.message.id, ()=> {
          roomsMap.set(request.message.id, {
              ...newRoom, 
              players: [
                ...newRoom.players, {
                    userId: socket.id, 
                    username: socket.handshake.query.username,
                    status: "notReady",
                    progress: 0,
                    time: 0
                  }
                ]
            }
          )
          updateRoomsListToAll(socket);
          updateSelectedRoomToAll(io, socket, request.message.id);
          io.to(socket.id).emit("Join room response", 
            {
              message: {
                room: roomsMap.get(request.message.id),
                roomName: request.message.id
              }
            })
        })
      }
      else {
        socket.emit("Error", {message: `${request.message.id} is full`});
      }
    });

    socket.on("Leave room request", data=>{
        leaveRoom(socket, data.message.prevRoomId);
        socket.leave(data.message.prevRoomId);
        updateRoomsListToAll(socket);
        updateSelectedRoomToAll(io, socket, data.message.prevRoomId, true);
    });

    socket.on("Create room request", data=>{
      if(roomsMap.has(data.message.roomName)) {
        socket.emit("Error", {message: `${data.message.roomName} is already exist`})
      }
      else {
        const createdRoom = createRoom(socket, data.message.roomName);
        updateRoomsListToAll(socket);
        socket.emit("Create room response", {
          message: {
            room: createdRoom,
            roomName: data.message.roomName
          }
        })
      }
    });

    socket.on("Player status request", data=>{
      const prevRoomState = roomsMap.get(data.message.roomName);
      const newPlayerState = prevRoomState.players.map(player=>{
        if(player.username === data.message.username){
          return {...player, status: data.message.status}
        }
        else return player
      });
      
      roomsMap.set(data.message.roomName, {...prevRoomState, players: newPlayerState});
      updateSelectedRoomToAll(io, socket, data.message.roomName);
    });

    socket.on("Game progress request", data=>{
      const prevRoomState = roomsMap.get(data.message.roomName);
      roomsMap.get(data.message.roomName).players.forEach((player, i)=>{
        if(player.username === data.message.username){
          prevRoomState.players[i].progress = data.message.progress
          if(roomsMap
            .get(data.message.roomName).players
            .every(player=>player.status==="ready") 
            && data.message.progress===100){
            prevRoomState.players[i].status = "winner";
            prevRoomState.players[i].time = data.message.time;
          }
          else if(data.message.progress===100) {
            prevRoomState.players[i].status = "done";
            prevRoomState.players[i].time = data.message.time;
          }
        }
      });
      
      roomsMap.set(data.message.roomName, prevRoomState);
      socket.emit("Selected Room update", {
        message: {
          room: roomsMap.get(data.message.roomName),
          roomName: data.message.roomName
        }
      });
      io.to(data.message.roomName).emit("Selected Room update", {
        message: {
          room: roomsMap.get(data.message.roomName),
          roomName: data.message.roomName
        }
      });
    });

    socket.on("Set player time request", data=>{
      console.log(data);
    })

    socket.on("disconnect", () => {
      userList.delete(socket.handshake.query.username);
      socket.disconnect(true);
    })
  });
};
