import { IRoom } from "../../types/socket.types";

import "../../styles/room.css"

interface IRenderRoom {
    room: IRoom;
    roomKey: string;
    onRoomClick: (id: string) => void;
}

const RenderRoom = ({room, roomKey, onRoomClick}: IRenderRoom) => {

    const onClick = () => {
        onRoomClick(roomKey)
    }

    return (
    <div className="room-preview">
        <div className="roomConnections">{room.players.length}/{room.maxPlayers} players</div>
        <div className="roomName">{roomKey}</div>
        <button 
            className="join-btn" 
            id={roomKey} 
            onClick={onClick}
        >
            Join room
        </button>
    </div>
    )
}

export default RenderRoom