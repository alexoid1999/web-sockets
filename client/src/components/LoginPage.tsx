import React, {useRef, useState} from "react";

import '../styles/login.css';
import '../styles/reset.css';
import '../styles/common.css';
import { useHistory } from "react-router-dom";

const LoginPage = () => {
    const history = useHistory();
    const [value, setValue] = useState("");
    const buttonRef = useRef<HTMLButtonElement>(null);

    const username = sessionStorage.getItem("username");
    if (username) {
     history.push("/game");
    }

    const onClickSubmitButton = () => {
        if (!value) {
            return;
        }
        sessionStorage.setItem("username", value);
        history.push("/game");
    };

    const onKeyUp = (ev: KeyboardEvent) => {
        if (ev.key === "Enter") {
            buttonRef.current && buttonRef.current.click();
        }
    };

    window.addEventListener("keyup", onKeyUp);

  return (
    <div id="login-page" className="full-screen flex-centered">
        <div className="flex">
            <div className="username-input-container">
                <input 
                    className="username-input" 
                    autoFocus 
                    placeholder="username" 
                    type="text" 
                    value={value}
                    onChange={(e)=> setValue(e.currentTarget.value)}
                    />
            </div>
            <button 
                ref={buttonRef}
                className="no-select"
                id="submit-button"
                onClick={onClickSubmitButton}
            >submit</button>
        </div>
    </div>
  );
}

export default LoginPage;
