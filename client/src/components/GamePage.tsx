import React, {useCallback, useEffect, useMemo, useState} from "react";

import '../styles/login.css';
import '../styles/reset.css';
import '../styles/common.css';
import { Redirect } from "react-router-dom";
import { IError, IGameTriggerResponseData, IRoom, IRoomList, IRoomsListResponseData, ISelectedRoom, ISelectedRoomResponseData } from "../types/socket.types";
import io from "socket.io-client";
import RenderRoom from "./helpers/renderRooms";

const GamePage = () => {
  const nullRoom: Map<string,IRoom> = new Map();
  const [rooms, setRooms] = useState(nullRoom);
  const [joinedRoom, setJoinedRoom] = useState<ISelectedRoom>();
  const [ready, setReady] = useState(true);
  const [startGame, setStartGame] = useState<IGameTriggerResponseData>({message:"pause"});

  const username = sessionStorage.getItem("username");
  if (!username) {
  return <Redirect to="/login" />;
  }

  const socket = useMemo(()=> io("", { query: { username } }),[username]);

  socket.on("Login Response", (data: IRoomsListResponseData)=>{
  setRooms(data.message.roomsList)
  })

  socket.on("Login Error", (data: IError)=>{
  alert(data.message);
  sessionStorage.clear();
  window.location.replace("/login");
  });

  socket.on("Error", (data: IError)=>alert(data.message));

  socket.on("Selected Room update", (data: ISelectedRoomResponseData)=>{
    setJoinedRoom(data.message)
  });

  socket.on("Join room response", (data: ISelectedRoomResponseData)=>{
    setJoinedRoom(data.message)
  });

  socket.on("Create room response", (data: ISelectedRoomResponseData)=>{
    setJoinedRoom(data.message)
  });

  socket.on("Set start game", (data: IGameTriggerResponseData)=>{
    if(startGame.message !== data.message && startGame.time !== data.time) {
       setStartGame(data);
    }
  })

  const onJoinRoom = (roomId: string) => {
    socket.emit("Join room request", {message: {user: username, id: roomId}})
  }

  const onLeaveRoom = () => {
    socket.emit("Leave room request", {message: {prevRoomId: joinedRoom?.roomName}})
    setJoinedRoom(undefined)
  }

  const onCreateRoom = () => {
    const roomName = prompt("Enter room name");
    if(roomName) {
      socket.emit("Create room request", {message: {roomName}})
    }
  }

  const onPlayerReady = () => {
    socket.emit("Player status request", {
      message: {
        roomName: joinedRoom?.roomName, 
        username, 
        status: ready? "ready": "notReady"
      }
    })
    setReady(prev=> !prev)
  }

  useEffect(()=>{ 
    if(startGame.text){
      const spans = document.querySelectorAll(".span");
      const handleKeyboardKey = (e: KeyboardEvent) =>{
        for (var index=0; index < spans.length; index++) {
          if(e.key.toLowerCase() === spans[index].innerHTML.toLowerCase()) {
            if(spans[index].classList.contains("ready-status-green")) {
              continue;
            }
            else if((!spans[index].classList.contains("ready-status-green") && spans[index-1] === undefined) 
                || (spans[index-1].classList.contains("ready-status-green"))){
                  spans[index].classList.add("ready-status-green");
                  spans[index].classList.remove("next");
                  socket.emit("Game progress request", {
                    message: {
                      progress: 100*((index+1)/spans.length),
                      username, 
                      roomName: joinedRoom?.roomName
                    }
                  })
                  if(index+1 <= spans.length){spans[index+1].classList.add("next")}
                  break;
            }
          }
        }
      }

      document.addEventListener('keydown', handleKeyboardKey)
      
      return ()=>{
        document.removeEventListener('keydown', handleKeyboardKey)
      }
    }
  },[startGame.text]);

  useEffect(()=>{
    let currentPlayer = joinedRoom?.room.players.find(player => player.username===username);
    if(currentPlayer && currentPlayer.status === "ready" && currentPlayer.progress === 100) {
      socket.emit("Set player time request", {message: {player: currentPlayer, time: startGame.time}})
    }
  },[startGame.time])

  return !joinedRoom ? 
    (
      <div id="rooms-page" className="flex-centered">
        <div className="rooms-list">
          {Array.from(rooms)
            .sort(([aKey,], [bKey,])=> aKey.toLocaleLowerCase() < bKey.toLocaleLowerCase() ? -1 : 1)
            .filter(([,val])=> !val.isGameStarted)
            .map(([key, value]) => {
              return <RenderRoom onRoomClick={onJoinRoom} room={value} roomKey={key}/>}
              )
          }
        </div>
        <div className="full-width flex-centered">
          <button 
            id="add-room-btn" 
            onClick={onCreateRoom}
            >
              Create room
          </button>
        </div>
    </div>
    )
  : (
    <div id="game-page" className="full-screen">
        <div className="left-block">
            <div className="top-block">
              <p>{joinedRoom.roomName}</p>
              {
                !(startGame.message==="start" || startGame.message==="set") &&
                <button id="quit-room-btn" onClick={onLeaveRoom}>Back to Rooms</button>
              }
            </div>
            <div className="user-list">
                {joinedRoom.room.players.map(player=> {
                  const status = player.status === "notReady"
                    ? "ready-status-red"
                    : player.status === "ready" 
                    ? "ready-status-green"
                    : player.status === "done"
                    ? "ready-status-done"
                    : player.status === "winner"
                    ? "ready-status-winner"
                    : "ready-status-red";

                  
                  return(
                    <div className="user-card">
                      <div className="user-info">
                        <span className={`status ${status}`}></span>
                        <p>{player.username} {player.username===username && "(you)"}</p>
                      </div>
                      <div className={`user-progress ${player.username}`}>
                        <div 
                          className={`progress ${status}`} 
                          style={{width: `${player.progress}%` || "0px"}}>

                        </div>
                      </div>
                    </div>
                  )})
                }
            </div>
        </div>
        <div className="right-block">
            <div className="timer-block">{startGame.message==="start" && startGame.time}</div>
            <div className="text-container">
              {startGame.message==="start" && startGame.text &&
                <p>{
                  startGame.text
                  .split("")
                    .map(word=><span className="span">{word}</span>)}
                </p>
                }
              {startGame.message==="set" && <p id="timer">{startGame.time}</p>}
              {(startGame.message==="finish" || startGame.message==="pause") &&
                <button 
                  id="ready-btn" 
                  onClick={onPlayerReady}
                >
                  {ready ? "Ready": "Not Ready"}
                </button>
              }
            </div>
        </div>
    </div>
    )
}

export default GamePage;
