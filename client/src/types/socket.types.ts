export interface IPlayer {
    userId: string;
    username: string;
    status: "notReady" | "ready" | "done" | "winner";
    progress: number;
    time?: number;
}

export interface IRoom {
    players: IPlayer[];
    maxPlayers: number;
    isGameStarted: boolean;
}

export interface IRoomList {
    key: string;
    room: IRoom
};

interface IRoomListResponse {
    roomsList: Map<string,IRoom>;
}

export interface ISelectedRoom {
    roomName: string;
    room: IRoom;
}

export interface IRoomsListResponseData {
    message: IRoomListResponse
}

export interface ISelectedRoomResponseData {
    message: ISelectedRoom
}

export interface IGameTriggerResponseData {
    message: "start" | "pause" | "set" | "finish";
    time?: number;
    text?: string
}

export interface IError {
    message: string;
}