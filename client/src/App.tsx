import React from "react";
import { BrowserRouter, Switch, Route, Redirect } from "react-router-dom";

import './App.css';
import GamePage from "./components/GamePage";

import LoginPage from "./components/LoginPage";

const App = () => {
  return (
    <BrowserRouter>
      <Switch>
        <Route exact={true} path="/login" component={LoginPage}/>
        <Route exact={true} path="/game" component={GamePage}/>
        <Route exact={true} path="*" render={()=><Redirect to="/login"/>}/>
      </Switch>
    </BrowserRouter>
  );
}

export default App;
