import express from "express";
import http from "http";
import path from "path";
import socketIO from "socket.io";
import socketHandler from "./socket";
import { STATIC_PATH, PORT } from "./config";

const app = express();
const httpServer = http.Server(app);
const io = socketIO(httpServer);

app.use(express.static(STATIC_PATH));

app.get("/*", (req, res) => {
  res.sendFile(path.join(STATIC_PATH, "index.html"));
});

socketHandler(io);

httpServer.listen(PORT, () => {
  console.log(`Listen server on port ${PORT}`);
});
