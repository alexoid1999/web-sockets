import textRoutes from "./textRoutes";


export default app => {
    app.use("/game/texts", textRoutes);
  };