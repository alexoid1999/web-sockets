import path from "path";

export const STATIC_PATH = path.join(__dirname, './client/build');

export const PORT = process.env.PORT || 3001;
